import React, { useState, useEffect } from 'react';

function AllIDT() {
    const [todos, setTodos] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
  
    useEffect(() => {
      const fetchData = async () => {
        try {
          const response = await fetch('http://jsonplaceholder.typicode.com/todos');
          if (!response.ok) {
            throw new Error('Failed to fetch data');
          }
          const data = await response.json();
          setTodos(data);
          setIsLoading(false);
        } catch (error) {
          console.error('Error fetching todos:', error);
          setIsLoading(false);
        }
      };
  
      fetchData();
    }, []);

    const renderTodoList = (filteredTodos) => {
        return filteredTodos.map(todo => (
          <li key={todo.id}>
            ID: {todo.id} - {todo.title}
          </li>
        ));
      };

      return (
        <div>
          <h1>To Do List</h1>
          {isLoading ? (
            <p>Loading...</p>
          ) : (
            <>
                <h2>List of All To Dos (IDs and Titles)</h2>
                <ul>{renderTodoList(todos)}</ul>
            </>
          )}
        </div>
      );
}

export default AllIDT;


















