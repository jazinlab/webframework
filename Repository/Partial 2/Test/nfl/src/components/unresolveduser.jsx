import React, { useState, useEffect } from 'react';

function UnresolvedUser() {
    const [todos, setTodos] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
  
    useEffect(() => {
      const fetchData = async () => {
        try {
          const response = await fetch('http://jsonplaceholder.typicode.com/todos');
          if (!response.ok) {
            throw new Error('Failed to fetch data');
          }
          const data = await response.json();
          setTodos(data);
          setIsLoading(false);
        } catch (error) {
          console.error('Error fetching todos:', error);
          setIsLoading(false);
        }
      };
  
      fetchData();
    }, []);

    const unresolvedTodos = todos.filter(todo => !todo.completed);

    const renderTodoList = (filteredTodos) => {
        return filteredTodos.map(todo => (
          <li key={todo.id}>
            ID: {todo.id} - UserId: {todo.userId}
          </li>
        ));
      };

      return (
        <div>
          <h1>To Do List</h1>
          {isLoading ? (
            <p>Loading...</p>
          ) : (
            <>
                <h2>List of Unresolved To Dos (IDs and UserIDs)</h2>
                <ul>{renderTodoList(unresolvedTodos)}</ul>
            </>
          )}
        </div>
      );
}

export default UnresolvedUser;