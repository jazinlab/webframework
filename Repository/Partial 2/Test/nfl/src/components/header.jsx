import React from 'react';
import logo from '../assets/nfl.png';

const Header = () => {
  const headerStyle = {
    backgroundColor: '#35682d',
    padding: '10px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  };

  const logoStyle = {
    width: '50px',
    alignSelf: 'center',
    cursor: 'pointer',
  };

  const textStyle = {
    color: 'white',
    fontSize: '24px',
    alignSelf: 'center',
    cursor: 'pointer',
  };

  return (
    <div style={headerStyle}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <img src={logo} alt="Logo" style={logoStyle}/>
        <h1 style={textStyle}>To Do APP</h1>
      </div>
    </div>
  );
};

export default Header;
