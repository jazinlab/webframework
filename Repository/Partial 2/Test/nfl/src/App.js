import React, { useState } from "react";
import Header from "./components/header";
import All from "./components/all";
import AllIDT from "./components/IDTitl";
import Unresolved from "./components/unresolvedid";
import Resolved from "./components/resolvedid";
import AllUser from "./components/allUser";
import ResolvedUser from "./components/resolvedUser";
import UnresolvedUser from "./components/unresolveduser";
import "./App.css";

function App() {
  const [showAll, setShowAll] = useState(false);
  const [showAllIDT, setShowAllIDT] = useState(false);
  const [showUnr, setShowUnr] = useState(false);
  const [showResolved, setShowResolved] = useState(false);
  const [showAllUser, setShowAllUser] = useState(false);
  const [showResolvedUser, setShowResolvedUser] = useState(false);
  const [showUnresolvedUser, setShowUnresolvedUser] = useState(false);

  const handleClickAll = () => {
    setShowAll(!showAll);
    setShowAllIDT(false);
    setShowUnr(false);
    setShowResolved(false);
    setShowAllUser(false);
    setShowResolvedUser(false);
    setShowUnresolvedUser(false);
  };

  const handleClickAllIDT = () => {
    setShowAllIDT(!showAllIDT);
    setShowAll(false);
    setShowUnr(false);
    setShowResolved(false);
    setShowAllUser(false);
    setShowResolvedUser(false);
    setShowUnresolvedUser(false);
  };

  const handleClickUnr = () => {
    setShowUnr(!showUnr);
    setShowAll(false);
    setShowAllIDT(false);
    setShowResolved(false);
    setShowAllUser(false);
    setShowResolvedUser(false);
    setShowUnresolvedUser(false);
  };

  const handleClickResolved = () => {
    setShowResolved(!showResolved);
    setShowAll(false);
    setShowAllIDT(false);
    setShowUnr(false);
    setShowAllUser(false);
    setShowResolvedUser(false);
    setShowUnresolvedUser(false);
  };

  const handleClickAllUser = () => {
    setShowAllUser(!showAllUser);
    setShowAll(false);
    setShowAllIDT(false);
    setShowUnr(false);
    setShowResolved(false);
    setShowResolvedUser(false);
    setShowUnresolvedUser(false);
  };

  const handleClickResolvedUser = () => {
    setShowResolvedUser(!showResolvedUser);
    setShowAll(false);
    setShowAllIDT(false);
    setShowUnr(false);
    setShowResolved(false);
    setShowAllUser(false);
    setShowUnresolvedUser(false);
  };

  const handleClickUnresolvedUser = () => {
    setShowUnresolvedUser(!showUnresolvedUser);
    setShowAll(false);
    setShowAllIDT(false);
    setShowUnr(false);
    setShowResolved(false);
    setShowAllUser(false);
    setShowResolvedUser(false);
  };

  return (
    <div className="App">
      <header className="App-header">
        <Header />
      </header>
      <br/>
      {showAll ? (
        <>
          <button className="back" onClick={handleClickAll}>Back</button>
          <All />
        </>
      ) : (
        <button className="button" onClick={handleClickAll}>All To Do ID</button>
      )}
      {showAllIDT ? (
        <>
          <button className="back" onClick={handleClickAllIDT}>Back</button>
          <AllIDT />
        </>
      ) : (
        <button className="button" onClick={handleClickAllIDT}>All To Do Title</button>
      )}
      {showUnr ? (
        <>
          <button className="back" onClick={handleClickUnr}>Back</button>
          <Unresolved />
        </>
      ) : (
        <button className="button" onClick={handleClickUnr}>Unresolved by ID</button>
      )}
      {showResolved ? (
        <>
          <button className="back" onClick={handleClickResolved}>Back</button>
          <Resolved />
        </>
      ) : (
        <button className="button" onClick={handleClickResolved}>Resolved by ID</button>
      )}
      {showAllUser ? (
        <>
          <button className="back" onClick={handleClickAllUser}>Back</button>
          <AllUser />
        </>
      ) : (
        <button className="button" onClick={handleClickAllUser}>All Users</button>
      )}
      {showResolvedUser ? (
        <>
          <button className="back" onClick={handleClickResolvedUser}>Back</button>
          <ResolvedUser />
        </>
      ) : (
        <button className="button" onClick={handleClickResolvedUser}>Resolved Users</button>
      )}
      {showUnresolvedUser ? (
        <>
          <button className="back" onClick={handleClickUnresolvedUser}>Back</button>
          <UnresolvedUser />
        </>
      ) : (
        <button className="button" onClick={handleClickUnresolvedUser}>Unresolved Users</button>
      )}
    </div>
  );
}

export default App;
