import { useState, useEffect } from 'react';
import './App.css';

const url = "https://api.coindesk.com/v1/bpi/currentprice.json"

function App() {
  const [data, setData]  = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect[() => {
    fetch(url)
      .then(response => response.json())
      .then(result => {
        setIsLoading(false);
        setData(result);
      })
  }, []]

  const getContent = () => {
    if (isLoading) {
      return(
        <div className="App">
          <h4>loading...</h4>
          <progress value ={null}/>
        </div>
      )
    }
    if (error) {
      return <h4>ERROR</h4>
    }
    return (
      <div className="App">4
      
      <h1>BTC TO USD</h1>
      <h3>BTC TO USD</h3>
      <table class="table table-striped">
        <thead>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
        </thead>
        <tbody>
          <tr>
            <id>{data["bpi"]["USD"].rate}</id>
            <id>{data["bpi"]["USD"].rate.float}</id>
            <id>{data["bpi"]["USD"].description}</id>
            <id>{data["time"].updated}</id>
          </tr>
        </tbody>
      </table>
      
      
      
      
      
      </div>  
    );
  }

  return (
    <div className="App">
      getContent()
    </div>
  );
}

export default App;

