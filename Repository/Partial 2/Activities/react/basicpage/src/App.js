import Header from './components/header';
import Carousel from './components/carrousel';
import Form from './components/form';
function App() {
  return (
    <div className="App">
      <Header/>
      <Carousel/>
      <Form/>
    </div>
  );
}

export default App;
