import React from 'react';
import logo from '../assets/images/nerfthis.png'
const Header = () => {
  const headerStyle = {
    backgroundColor: '#800080',
    padding: '10px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  };
  const textStyle = {
    color: 'white',

  }
  const optionStyle = {
    color: 'white',
    border: '1px solid white',
    borderRadius: '5px',
    padding: '8px 15px',
    background: 'none',
    cursor: 'pointer',
    transition: 'color 0.3s',
  };

  const handleMouseEnter = (event) => {
    event.target.style.color = '#660066';
    event.target.style.background = 'white';
    
  };

  const handleMouseLeave = (event) => {
    event.target.style.color = 'white';
    event.target.style.background = 'none';
  };

  return (
    <div style={headerStyle}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <img src={logo} alt="Logo" style={{ width: '50px', marginRight: '10px' }} />
        <h1 style={{ ...textStyle, fontSize: '24px', alignSelf: 'center' }}>Nerf This: Eshop</h1>
      </div>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <button style={{ ...optionStyle, fontSize: '16px', margin: '0', alignSelf: 'center' }} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
          Inicio
        </button>
        <button style={{ ...optionStyle, fontSize: '16px', margin: '0', alignSelf: 'center' }} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
          About Us
        </button>
        <button style={{ ...optionStyle, fontSize: '16px', margin: '0', alignSelf: 'center' }} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
          Tienda
        </button>
      </div>
    </div>
  );
};

export default Header;