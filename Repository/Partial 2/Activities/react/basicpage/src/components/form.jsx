import React, { useState } from "react";

export default function Form() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const fullName = firstName + " " + lastName;

  function handleFirstNameChange(e) {
    setFirstName(e.target.value);
  }

  function handleLastNameChange(e) {
    setLastName(e.target.value);
  }

  return (
    <form style={{ backgroundColor: 'gray', padding: '20px', borderRadius: '10px', color: '#660066', alignItems: 'center', borderColor: '#80080'}}>
      <div className="mb-3">
        <label htmlFor="firstName" className="form-label">First Name</label>
        <input type="text" className="form-control" id="firstName" value={firstName} onChange={handleFirstNameChange} style={{ borderRadius: '5px' }} />
      </div>
      <div className="mb-3">
        <label htmlFor="lastName" className="form-label">Last Name</label>
        <input type="text" className="form-control" id="lastName" value={lastName} onChange={handleLastNameChange} style={{ borderRadius: '5px' }} />
      </div>
      <button type="submit" className="btn btn-primary" style={{ borderRadius: '5px', backgroundColor: '#800080', borderColor: '#800080', color: 'white' }}>
        Submit
      </button>
    </form>
  );
}
