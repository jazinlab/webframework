
import Nana from '../assets/images/Nana.jpeg';

export default function Image(props){
    return(
        <img src={Nana} style={styles.reSize}/>
    );
}

const styles = {
    reSize: {
        width: 320,
        height: 449,
        paddingTop: 58,
    },
};